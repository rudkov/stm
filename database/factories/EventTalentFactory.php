<?php

namespace Database\Factories;

use App\Models\Model;
use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\Event;
use App\Models\Talent;

class EventTalentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Model::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }

    public function run()
    {
        $events = Event::all();
        $talents = Talent::all();

        $groupedTalents = $talents->groupBy('team_id');

        foreach($events as $event) {
            $eventTalents = $groupedTalents[$event->team_id]->random(rand(0,3));

            foreach($eventTalents as $eventTalent) {
                $event->talents()->save($eventTalent);
            }
        }
    }
}
