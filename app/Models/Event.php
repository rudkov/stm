<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\EventType;
use App\Models\EventChunk;
use App\Models\Talent;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class Event extends Model
{
    use HasFactory;
    use HasUuids;
    use SoftDeletes;

    protected $hidden = [
        'team_id',
    ];

    protected $fillable = [
        'title',
    ];

    public function eventChunks()
    {
        return $this->hasMany(EventChunk::class)->orderBy('start_date')->orderBy('end_date');
    }

    public function eventType()
    {
        return $this->belongsTo(EventType::class);
    }

    public function talents()
    {
        return $this->belongsToMany(Talent::class)->orderBy('first_name')->orderBy('last_name');
    }
}
