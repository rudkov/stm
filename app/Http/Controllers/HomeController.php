<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Models\Language;
use App\Models\SocialMedia;
use App\Models\Talent;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\JoinClause;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    public function index()
    {
        $time_start = $this->microtime_float();

        $raw_talents = DB::table('talents')
            ->where('talents.team_id', Auth::user()->team->id)
            ->leftJoin('talent_emails', 'talents.id', '=', 'talent_emails.talent_id')
            ->leftJoin('email_types', 'email_types.id', '=', 'talent_emails.email_type_id')
            ->leftJoin('talent_phones', 'talents.id', '=', 'talent_phones.talent_id')
            ->leftJoin('phone_types', 'phone_types.id', '=', 'talent_phones.phone_type_id')
            ->orderBy('talents.first_name', 'asc')
            ->orderBy('talents.last_name', 'asc')
            ->orderBy('email_types.weight', 'desc')
            ->orderBy('phone_types.weight', 'desc')
            ->get(['talents.id', 'talents.first_name', 'talents.last_name', 'talents.current_location', 'talent_emails.info as email', 'email_types.weight as email_weight', 'talent_phones.info as phone', 'phone_types.weight as phone_weight']);

        $talents = array();
        foreach ($raw_talents as $talent) {
            $talents[$talent->id]['id'] = $talent->id;
            $talents[$talent->id]['name'] = trim($talent->first_name . ' ' . $talent->last_name);
            $talents[$talent->id]['location'] = $talent->current_location;
            $talents[$talent->id]['email'] = $talent->email;
            $talents[$talent->id]['phone'] = $talent->phone;
        }
        $talents = array_values($talents);

        $time_end = $this->microtime_float();
        $time = $time_end - $time_start;

        dump($time);
        dump($talents[0]);
        dump($talents[1]);
        dump($talents[2]);
        dump($talents[3]);
        dump($talents[4]);
    }
}
